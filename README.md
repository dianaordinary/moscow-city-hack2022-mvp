# Debunker
Проект состоит из Фронтовой части - DebunkerClient И Бэкэндовой(Отдельный сервис,написанный на питоне, занимающийсе обработкой и анализом "Добавленной" новости. Его в данном репозитории нет)

В связи отсутствием бэк-сервиса. В данном проекте отключен функционал авторизации( но он реализован).
И создан JSONplugService сервис с заглушкой для получения тестовых данных с бэк-сервиса.

`.assets` -  Содержит видео-презентация CodeMind - demunker.mov


# DebunkerClient

Проект создан с помощью Angular.


## Install DebunkerClient/Установить DebunkerClient
Запустите `npm install`. (Но прежде, установите `node.js` (рекомендую 16ую версии)

## Development DebunkerClient/Разворачивание DebunkerClient
Запустите приложение командой `ng serve`. Перейдите по `http://localhost:4200/`. 

## About DebunkerClient/О DebunkerClient
В приложении используется компоненты из `Angular material UI` и `Swiper`

Пару слов по структуре проекта</br>
`.app` - Содержит основное приложение (кастомные компоненты(страницы-page), страницы (analysis-page, article-dashboard, login-page, register-page, source-page и модальные окна и т.д.)
`.assets` -  Содержит стили, картинки и т.д.
`.proxy,environments` -  Содержит конфигарационные файлы

`.app/components` - Содержит кастомные шаблоны-компоненты,которые потом используются в кастомных компонентах
CardComponent - шаблон блоков(картачка новости и карточка ресурсы)</br>
CommentComponent - шаблон блока с комментариями</br>
LoaderComponent- шаблон прогрессбара</br>
GradeComponent - шаблон блока с Оценками Нейросети и Решением</br>
DecisionComponent - шаблон модального окна,которое вызывается из блока оценок нейросети</br>

`.app/shared/services` -   Содержит кастомные сервисы (auth,token - для авторизауции, debunker- для обращения на сервер, util- вспомогательный )




p.s. при просмотре на локальной машине
при необходимости можно исправить конфигурационный файл proxy.conf.json а именно поля target</br>
{</br>
"/api": {</br>
"target": "http://127.0.0.1:8000",</br>
"secure": false</br>
}</br>
}</br>


![](src/infoPic/1.png)

![](src/infoPic/2.png)

![](src/infoPic/3.png)

![](src/infoPic/4.png)

![](src/infoPic/5.png)

![](src/infoPic/6.png)

![](src/infoPic/7.png)

![](src/infoPic/8.png)

![](src/infoPic/9.png)

![](src/infoPic/10.png)
